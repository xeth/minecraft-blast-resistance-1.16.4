/*
 * Change block blast resistances, see:
 * https://bukkit.org/threads/changing-block-explosion-resistance.99277/
 * https://www.spigotmc.org/threads/looking-to-get-the-default-harness-of-vanilla-blocks.222543/
 * 
 * Implement bukkit plugin interface
 */

package xeth.blastresistance

import java.io.File
import org.bukkit.Bukkit
import org.bukkit.ChatColor
import org.bukkit.Material
import org.bukkit.entity.Player
import org.bukkit.plugin.java.JavaPlugin
import org.bukkit.configuration.ConfigurationSection
import org.bukkit.configuration.file.FileConfiguration
import org.bukkit.configuration.file.YamlConfiguration
import org.bukkit.command.Command
import org.bukkit.command.CommandExecutor
import org.bukkit.command.CommandSender
import org.bukkit.command.TabCompleter

internal object Message {

    public val PREFIX = "[BlastResistance]"
    public val COL_MSG = ChatColor.DARK_GREEN
    public val COL_ERROR = ChatColor.RED

    // print generic message to chat
    public fun print(sender: Any?, s: String) {
        if ( sender === null ) {
            System.out.println("${PREFIX} Message called with null sender: ${s}")
            return
        }

        val msg = "${COL_MSG}${s}"
        if ( sender is Player ) {
            sender.sendMessage(msg)
        }
        else {
            (sender as CommandSender).sendMessage(msg)
        }
    }

    // print error message to chat
    public fun error(sender: Any?, s: String) {
        if ( sender === null ) {
            System.out.println("${PREFIX} Message called with null sender: ${s}")
            return
        }

        val msg = "${COL_ERROR}${s}"
        if ( sender is Player ) {
            sender.sendMessage(msg)
        }
        else {
            (sender as CommandSender).sendMessage(msg)
        }
    }
}

public class BlastResistanceCommand(val plugin: BlastResistancePlugin) : CommandExecutor, TabCompleter {

    override fun onCommand(sender: CommandSender, cmd: Command, commandLabel: String, args: Array<String>): Boolean {
        
        val player = if ( sender is Player ) sender else null

        // no args, open flags gui
        if ( args.size == 0 ) {
            this.printHelp(sender)
            return true
        }

        // parse subcommand
        val arg = args[0].toLowerCase()
        when ( arg ) {
            "help" -> printHelp(sender)
            "list" -> list(sender)
            "revert" -> revert(sender)
            "reload" -> reload(sender)
        }

        return true
    }

    override fun onTabComplete(sender: CommandSender, command: Command, alias: String, args: Array<String>): List<String> {
        return listOf("help", "list", "revert", "reload")
    }

    private fun printHelp(sender: CommandSender?) {
        Message.print(sender, "[BlastResistance] block blast resistance overrides for Mineman 1.16.3")
        return
    }

    /**
     * Revert all blast resistances
     */
    private fun revert(sender: CommandSender?) {
        val player = if ( sender is Player ) sender else null
        if ( player === null || player.isOp() ) {
            plugin.revertAll()
            Message.print(sender, "[BlastResistance] reverted all blast resistance overrides")
        }
        else {
            Message.error(sender, "[BlastResistance] Only operators can reload")
        }
    }

    /**
     * Reload config
     */
    private fun reload(sender: CommandSender?) {
        val player = if ( sender is Player ) sender else null
        if ( player === null || player.isOp() ) {
            plugin.loadConfig()
            Message.print(sender, "[BlastResistance] reloaded")
        }
        else {
            Message.error(sender, "[BlastResistance] Only operators can reload")
        }
    }

    /**
     * List overrides
     */
    private fun list(sender: CommandSender?) {
        for ( blockOverride in plugin.blockOverrides ) {
            val mat = blockOverride.material
            // val block = blockOverride.block
            Message.print(sender, "${mat}: ${blockOverride.getField("durability")}")
        }
    }
}


public class BlastResistancePlugin : JavaPlugin() {

    // list of block overrides
    internal val blockOverrides: ArrayList<BlockOverride> = arrayListOf()

    /**
     * Load config, update settings
     * Returns number of flags loaded
     */
    public fun loadConfig() {
        // revert existing changes
        this.revertAll()

        // get config file
        val configFile = File(this.getDataFolder().getPath(), "config.yml")
        if ( !configFile.exists() ) {
            this.getLogger().info("No config found: generating default config.yml")
            this.saveDefaultConfig()
        }

        val config = YamlConfiguration.loadConfiguration(configFile)

        // load materials
        val materialNames = config.getKeys(false)
        for ( matName in materialNames ) {
            val mat = Material.matchMaterial(matName)
            if ( mat === null ) {
                continue
            }
            
            val resistance = config.getDouble(matName).toFloat()
            try {
                val blockOverride = BlockOverride(mat)
                blockOverride.setField("durability", resistance)
                this.blockOverrides.add(blockOverride)
            }
            catch ( e: Exception ) {
                System.err.println("[BlastResistance] Failed to set ${mat}: ${resistance}")
            }
        }
    }

    public fun revertAll() {
        for ( blockOverride in this.blockOverrides ) {
            blockOverride.revertAll()
        }

        this.blockOverrides.clear()
    }

    override fun onEnable() {
        // measure load time
        val timeStart = System.currentTimeMillis()

        val logger = this.getLogger()

        // load flags
        this.loadConfig()

        // register commands
        this.getCommand("blast")?.setExecutor(BlastResistanceCommand(this))
        
        // print load time
        val timeEnd = System.currentTimeMillis()
        val timeLoad = timeEnd - timeStart
        logger.info("Enabled in ${timeLoad}ms")

        // print success message
        logger.info("now this is epic")
    }

    override fun onDisable() {
        for ( blockOverride in this.blockOverrides ) {
            blockOverride.revertAll()
        }
        logger.info("wtf i hate xeth now")
    }
}

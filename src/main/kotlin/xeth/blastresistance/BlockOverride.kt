/**
 * Original java version:
 * https://gist.github.com/aadnk/3642791
 * 
 * Modified for kotlin
 * 
 * Uses reflection to get spigot block to set durability on
 * 
 * Tested for 1.16
 */

package xeth.blastresistance

import java.lang.reflect.Field
import java.lang.reflect.Method
import java.util.HashMap
import org.apache.commons.lang3.reflect.FieldUtils
import org.bukkit.Material
import org.bukkit.Bukkit

// if using built-in versioning, uncomment and set right version
// import org.bukkit.craftbukkit.v1_16_R3.util.CraftMagicNumbers
// import net.minecraft.server.v1_16_R3.Block

private fun getNMSClass(name: String): Class<*> {
    val version = Bukkit.getServer().javaClass.getPackage().getName().split(".")[3]
    return Class.forName("net.minecraft.server." + version + "." + name);
}

private fun getCraftBukkitClass(name: String): Class<*> {
    val version = Bukkit.getServer().javaClass.getPackage().getName().split(".")[3]
    return Class.forName("org.bukkit.craftbukkit." + version + "." + name);
}

/**
 * Overrides particular values on blocks,
 * 
 * Original author:
 * @author Kristian
 */
public class BlockOverride(
    val material: Material // the block we will override
) {
    
    // val block: Block = CraftMagicNumbers.getBlock(material) // without NMS reference

    val block = BlockOverride.getBlock(material)

    // old values
    private val oldValues: HashMap<String, Any> = HashMap<String, Any>()
    
    /**
     * Update the given field with a new value.
     * @param fieldName - name of field.
     * @param value - new value.
     * @throws IllegalArgumentException If the field name is NULL or the field doesn't exist.
     * @throws RuntimeException If we don't have security clearance.
     */
    public fun setField(fieldName: String, value: Any?) {
        try {
            // store old value
            if ( !oldValues.contains(fieldName) ) {
                val current = this.getField(fieldName)
                this.oldValues.put(fieldName, current)
            }
            // write the value directly
            FieldUtils.writeField(this.getFieldFromName(fieldName), this.block, value)
        } catch (e: IllegalAccessException) {
            throw RuntimeException("Unable to read field.", e)
        }
    }
    
    /**
     * Retrieves the current field value.
     * @param fieldName - name of field.
     * @throws IllegalArgumentException If the field name is NULL or the field doesn't exist.
     * @throws RuntimeException If we don't have security clearance.
     */
    public fun getField(fieldName: String): Any {
        try {
            // Read the value directly
            return FieldUtils.readField(this.getFieldFromName(fieldName), block)
        } catch (e: IllegalAccessException) {
            throw RuntimeException("Unable to read field.", e)
        }
    }
    
    /**
     * Retrieves the old vanilla field value.
     * @param fieldName - name of field.
     * @throws IllegalArgumentException If the field name is NULL or the field doesn't exist.
     * @throws RuntimeException If we don't have security clearance.
     */
    public fun getVanilla(fieldName: String): Any? {
        if ( oldValues.containsKey(fieldName) ) {
            return oldValues.get(fieldName)
        } else {
            return this.getField(fieldName)
        }
    }
    
    /**
     * Retrieves copy of stored vanilla values
     * @return Old values.
     */
    public fun getVanillaValues(): HashMap<String, Any> {
        return HashMap(this.oldValues)
    }
    
    /**
     * Reset everything to vanilla.
     */
    public fun revertAll() {
        // Reset what we have
        for ( (stored, _) in this.oldValues ) {
            this.setField(stored, this.getVanilla(stored))
        }
        
        // Remove list
        this.oldValues.clear()
    }
    
    /**
     * Called when we wish to persist every change, even when this class is garbage collected.
     */
    public fun saveAll() {
        this.oldValues.clear()
    }
    
    protected fun finalize() {
        // We definitely should revert when we're done
        if ( this.oldValues.size > 0) {
            this.revertAll()
        }
    }
    
    private fun getFieldFromName(fieldName: String): Field {
        val blockField = FieldUtils.getField(this.block.javaClass, fieldName, true)
        if ( blockField === null ) {
            throw IllegalArgumentException("Cannot locate field ${fieldName}")
        }
        return blockField
    }


    companion object {
        // store craft bukkit class method to get block
        val getBlockMethod: Method = getCraftBukkitClass("util.CraftMagicNumbers").getMethod("getBlock", Material::class.java)

        public fun getBlock(material: Material): Any {
            return getBlockMethod.invoke(null, material)
        }
    }
}